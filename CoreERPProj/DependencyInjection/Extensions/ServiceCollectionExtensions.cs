﻿using CoreERPProj.Repositories.Branches;
using CoreERPProj.Repositories.BusinessPartners;
using CoreERPProj.Repositories.UserAccounts;

namespace CoreERPProj.DependencyInjection.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection ServiceCollection(this IServiceCollection services, IConfiguration config)
        {
            //Branches
            services.AddTransient<IUserAccounts, UserAccountsRepo>();
            services.AddTransient<IBranches, BranchesRepo>();
            services.AddTransient<ICustomers, CustomersRepo>();
            return services;
        }
    }
}
