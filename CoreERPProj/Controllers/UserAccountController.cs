﻿using CoreERPProj.Repositories.UserAccounts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace CoreERPProj.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAccountController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IUserAccounts _userAccountsService;
        public UserAccountController(ILogger<UserAccountController> logger, IUserAccounts userAccountsService)
        {
            _logger = logger;
            _userAccountsService = userAccountsService;
        }
        [HttpGet("getAllUserAccounts")]
        public async Task<IActionResult> GetAllUserAccounts() 
        {
            try
            {
                var dt = await _userAccountsService.GetAllUserAccountsAsync();
                // var user = _userAccountsService.GetAllUserAccounts();
                return Ok(dt);
            }
            catch
            {
                throw;
            }
        }
        [HttpGet("getUserAccountTempate")]
        public async Task<IActionResult> GetUserAccountTempate()
        {
            try{
                return Ok(await _userAccountsService.GetUserAccountTempate());
            }
            catch{
                throw;
            }
        }
    }
}
