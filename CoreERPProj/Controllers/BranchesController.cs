﻿using CoreERPProj.DataApp;
using CoreERPProj.Models.Branches;
using CoreERPProj.Repositories.Branches;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Data;
using System.Xml;

namespace CoreERPProj.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        private readonly ILogger<BranchesController> _logger;
        private readonly IBranches _branchesService;
        public BranchesController(ILogger<BranchesController> logger,IBranches branchesService) 
        {
            _logger = logger;
            _branchesService = branchesService;
        }
        [HttpGet("GetAllBranchesAsync")]
        public IActionResult GetAllBranchesAsync()
        {
            try
            {               
                return Ok(_branchesService.GetAllBranches());
            }
            catch 
            {
                throw;
            }
        }
    }
}
