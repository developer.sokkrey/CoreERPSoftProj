﻿using CoreERPProj.Models.BusinessPartners;
using CoreERPProj.Repositories.BusinessPartners;
using CoreERPProj.Repositories.UserAccounts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using IActionResult = Microsoft.AspNetCore.Mvc.IActionResult;

namespace CoreERPProj.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomers _customers;
        public CustomerController(ICustomers customers)
        {
            _customers = customers;
        }
        [HttpPost]
        public async Task<IActionResult> CreateCustomer([FromBody] List<Customer> customer)
        {
            _customers.CreateCustomerList(customer);
            return Ok();
        }
    }
}
