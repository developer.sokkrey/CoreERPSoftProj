﻿using CoreERPProj.Utilities.Extionsions;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Data;

namespace CoreERPProj.DataApp
{
    public interface IQueryContext
    {
        IEnumerable<TEntity> FromSqlRaw<TEntity>(Action<SqlCommand> onSqlCommand)
           where TEntity : class, new();
        Task<IEnumerable<TEntity>> FromSqlRawAsync<TEntity>(Action<SqlCommand> onSqlCommand)
           where TEntity : class, new();
        IEnumerable<TEntity> FromSqlRaw<TEntity>(string commandText, Action<SqlParameterCollection> actionSqlParam = null)
           where TEntity : class, new();
        Task<IEnumerable<TEntity>> FromSqlRawAsync<TEntity>(string commandText, Action<SqlParameterCollection> actionSqlParam = null)
            where TEntity : class, new();
        SqlCommand ExecuteSqlRaw(string commandText, Action<SqlCommand> onSqlCommand);
        SqlCommand ExecuteSqlRaw(Action<SqlCommand> onSqlCommand);
        SqlConnection NewSqlConnection(string connectionString = "");
        SqlCommand FromSqlCommand(SqlConnection connection, string tableName);
        SqlCommand FromSqlCommand(SqlConnection connection, Type entityType);
        IEnumerable<TEntity> Query<TEntity>(string tableName) where TEntity : class, new();
        IEnumerable<TEntity> Query<TEntity>() where TEntity : class, new();
        IEnumerable<Dictionary<string, object>> ToDictionaries(string tableName);
        IEnumerable<Dictionary<string, object>> ToDictionaries(Type typeOfEntity);
        Task<IEnumerable<Dictionary<string, object>>> ToDictionariesAsync(string tableName);
        Task<IEnumerable<Dictionary<string, object>>> ToDictionariesAsync(Type typeOfEntity);
        string GetTableName(Type entityType);
    }
    public class QueryContext : IQueryContext
    {
        private readonly ILogger<QueryContext> _logger;
        private readonly DataContext _dataContext;
        private readonly string _connectionString;

        public QueryContext(ILogger<QueryContext> logger, DataContext dataContext)
        {
            _logger = logger;
            _dataContext = dataContext;
            _connectionString = dataContext.Database.GetConnectionString();
        }
        public virtual SqlConnection NewSqlConnection(string connectionString = "")
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = _connectionString;
            }
            return new SqlConnection(connectionString);
        }
        public IEnumerable<TEntity> FromSqlRaw<TEntity>(Action<SqlCommand> onSqlCommand) where TEntity : class, new()
        {
            using var sqlConnection = new SqlConnection(_connectionString);
            sqlConnection.Open();
            using var command = new SqlCommand();
            onSqlCommand.Invoke(command);
            command.Connection ??= sqlConnection;
            using var reader = command.ExecuteReader();
            return reader.Set<TEntity>();
        }
        public async Task<IEnumerable<TEntity>> FromSqlRawAsync<TEntity>(Action<SqlCommand> onSqlCommand)
          where TEntity : class, new()
        {
            using var sqlConnection = new SqlConnection(_connectionString);
            await sqlConnection.OpenAsync();
            using var command = new SqlCommand();
            onSqlCommand.Invoke(command);
            command.Connection ??= sqlConnection;
            using var reader = await command.ExecuteReaderAsync();
            return reader.Set<TEntity>();
        }
        public IEnumerable<TEntity> FromSqlRaw<TEntity>(string commandText, Action<SqlParameterCollection> onSqlParameters = null)
           where TEntity : class, new()
        {
            using var sqlConnection = new SqlConnection(_connectionString);
            sqlConnection.Open();
            using var command = new SqlCommand(commandText, sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            onSqlParameters?.Invoke(command.Parameters);
            using var reader = command.ExecuteReader();
            return reader.Set<TEntity>();
        }

        public async Task<IEnumerable<TEntity>> FromSqlRawAsync<TEntity>(
            string commandText, Action<SqlParameterCollection> onSqlParameters = null)
            where TEntity : class, new()
        {
            using var sqlConnection = new SqlConnection(_connectionString);
            await sqlConnection.OpenAsync();
            using var command = new SqlCommand(commandText, sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            onSqlParameters?.Invoke(command.Parameters);
            using var reader = await command.ExecuteReaderAsync();
            return reader.Set<TEntity>();
        }
        public SqlCommand ExecuteSqlRaw(string commandText, Action<SqlCommand> onSqlCommand)
        {
            var connection = new SqlConnection(_connectionString);
            connection.Open();
            var command = new SqlCommand(commandText, connection);
            command.CommandType = CommandType.StoredProcedure;
            onSqlCommand.Invoke(command);
            command.ExecuteNonQuery();
            return command;
        }
        public SqlCommand ExecuteSqlRaw(Action<SqlCommand> onSqlCommand)
        {
            var connection = new SqlConnection(_connectionString);
            connection.Open();
            var command = new SqlCommand();
            command.Connection = connection;
            onSqlCommand.Invoke(command);
            command.ExecuteNonQuery();
            return command;
        }
        public SqlCommand FromSqlCommand(SqlConnection connection, string tableName)
        {
            connection.Open();
            var command = new SqlCommand($"uspSelectTable", connection);
            command.Parameters.AddWithValue("@tableName", tableName);
            command.CommandType = CommandType.StoredProcedure;
            return command;
        }
        public string GetTableName(Type entityType)
        {
            IAnnotation annotation = _dataContext.Model.FindEntityType(entityType).FindAnnotation("Relational:TableName");
            if (annotation == null)
            {
                _logger.LogError($"Table annotation of {entityType.Name} is not found.");
                return string.Empty;
            }
            return $"{annotation.Value}";
        }
        public SqlCommand FromSqlCommand(SqlConnection connection, Type entityType)
        {
            var tableName = GetTableName(entityType);
            return FromSqlCommand(connection, tableName);
        }
        public IEnumerable<TEntity> Query<TEntity>(string tableName) where TEntity : class, new()
        {
            using var conn = new SqlConnection(_connectionString);
            var reader = FromSqlCommand(conn, tableName).ExecuteReader();
            return reader.Set<TEntity>();
        }
        public IEnumerable<TEntity> Query<TEntity>() where TEntity : class, new()
        {
            var tableName = GetTableName(typeof(TEntity));
            return Query<TEntity>(tableName);
        }
        public IEnumerable<Dictionary<string, object>> ToDictionaries(string tableName)
        {
            using var conn = new SqlConnection(_connectionString);
            var reader = FromSqlCommand(conn, tableName).ExecuteReader();
            return reader.ToDictionaries();
        }
        public IEnumerable<Dictionary<string, object>> ToDictionaries(Type typeOfEntity)
        {
            string tableName = GetTableName(typeOfEntity);
            return ToDictionaries(tableName);
        }
        public Task<IEnumerable<Dictionary<string, object>>> ToDictionariesAsync(Type typeOfEntity)
        {
            string tableName = GetTableName(typeOfEntity);
            return ToDictionariesAsync(tableName);
        }

        public async Task<IEnumerable<Dictionary<string, object>>> ToDictionariesAsync(string tableName)
        {
            using var conn = new SqlConnection(_connectionString);
            var reader = await FromSqlCommand(conn, tableName).ExecuteReaderAsync();
            var dict = reader.ToDictionaries();
            return dict;
        }
    }
}
