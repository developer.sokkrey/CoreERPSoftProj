﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Data;

namespace CoreERPProj.Utilities.Extionsions
{
    public static class QueryExtensions
    {
        public static IEnumerable<TEntity> Set<TEntity>(this IDataReader reader) where TEntity : new()
        {
            ICollection<TEntity> entitySet = new List<TEntity>();
            while (reader.Read())
            {
                TEntity obj = new TEntity();
                for (int i = 0; i < reader.FieldCount; i++ )
                {
                    string key = reader.GetName(i);
                    object value = reader.GetValue(i);
                    EntityHelper.SetProperty(obj, key, value);
                }
                entitySet.Add(obj);
            } 
            return entitySet;
        }
        public static IEnumerable<Dictionary<string, object>> ToDictionaries(this IDataReader reader)
        {
            ICollection<Dictionary<string, object>> entitySet = new List<Dictionary<string, object>>();
            while (reader.Read())
            {
                Dictionary<string, object> obj = new Dictionary<string, object>();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string key = reader.GetName(i);
                    object value = reader.GetValue(i);
                    obj.Add(key, value);
                }
                entitySet.Add(obj);
            }
            return entitySet;
        }
    }
}
