using CoreERPProj.Models.Companies;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace CoreERPProj.Models.UserAccounts
{
    [Index(nameof(Code), IsUnique = true)]
    public class Department
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid DeparmentID { get; set; } //DeparmentID
        public required string Code {get;set;} //DepartmentCode
        public required string Name { get; set; } //DepartmentName
        public Guid CompID { get; set; } //CompanyID
        public bool Enable {get; set;} //Enable
        //----------------ForeignKey-------------------------
        [ForeignKey(nameof(CompID))]
        public required Company Company { get; set; }
    }      
}