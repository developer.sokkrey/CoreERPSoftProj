﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CoreERPProj.Models.Companies;
using CoreERPProj.Models.Branches;
using static CoreERPProj.Model.EnumService.EnumServices;
using CoreERPProj.Models.UsersPrivilege;
using Microsoft.EntityFrameworkCore;

namespace CoreERPProj.Models.UserAccounts
{
    [Index(nameof(Code), IsUnique = true)]
    public class UserAccount
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserAccountID { get; set; } //UserAccountID
        public required string Code {get; set;} // EmployeeCode
        public required string Name {get;set;} //EmployeeName
        public required string Username { get; set; } //Username
        public string? PasswordHash { get; set; } //PasswordHash
        public Guid RuleID { get; set; } //RuleID
        public Guid CompID { get; set; } //CompanyID
        public Guid BranchID { get; set; } //BranchID
        public Genders Gender{ get; set; } //Gender
        public UserStatus Status { get; set; } //Status
        public Guid DepartmentID { get;set; } //DepartmentID
        //----------------NotMapped-------------------------
        [NotMapped]
        public string? Password { get; set; }
        [NotMapped]
        public string? ConfirmPassword { get; set; }
        //----------------ForeignKey-------------------------
        [ForeignKey(nameof(RuleID))]
        public required Rule Rule { get; set; }
        [ForeignKey(nameof(CompID))]
        public required Company Company { get; set; }
        [ForeignKey(nameof(BranchID))]
        public required Branch Branch { get; set; }
        [ForeignKey(nameof(DepartmentID))]
        public required Department Department { get; set; }

    }      
}
