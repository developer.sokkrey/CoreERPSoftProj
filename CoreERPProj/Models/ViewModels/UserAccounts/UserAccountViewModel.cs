﻿namespace CoreERPProj.Models.ViewModels.UserAccounts
{
    public class UserAccountViewModel
    {
        public Guid UserAcccountID { get; set; }
        public required string Code { get; set;}
        public string? Name { get; set;}
        public string? RuleName {  get; set;}
        public string? CompanyName {  get; set;}
        public string? BranchName { get; set;}
        public int Gender { get; set; }
        public string? GenderValue { get; set;} 
        public int Status { get; set; }
        public string? StatusValue { get; set;}
        public required string DepartmentName {  get; set; }
    }
}
