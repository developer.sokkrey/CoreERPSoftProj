﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoreERPProj.Models.UsersPrivilege
{
    public class RulePrivilege
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid RulePrivID { get; set; } //RulePrivilegeId
        public Guid RuleID { get; set; } //RuleID
        public Guid FunctID { get; set; } //FunctionID
        public bool Enable { get; set; } = true; //Enable
        //----------------ForeignKey-------------------------
        [ForeignKey(nameof (RuleID))]
        public required Rule Rule { get; set; }
        [ForeignKey(nameof (FunctID))]
        public required Function Function { get; set; }
    }
}
