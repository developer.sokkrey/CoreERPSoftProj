// import axios from 'axios'
import { defineStore } from 'pinia'
// import { json } from 'stream/consumers';
// import { fetchUserLists } from '~/src/service';
export const useUserAccountStore = defineStore('user-account', {
  state: () => ({
    userList: [],
    genders:[],
    rules:[],
    branches:[],
    departments:[],
    statuses:[],
  }),
  getters: {
    
  },
  actions: {
    async getUserAccount() {
      // axios.get("http://localhost:5121/api/UserAccount/GetAllUserAccounts").then(response => {
      //   const result = response.data

      //   this.userList = response.data
      //   console.log(response.data);
      // })
      // const result = await fetchUserLists();
      // console.log('result', result.data);
      // console.log("userAccount", result);
      const result = await fetch('http://localhost:5121/api/userAccount/getalluseraccounts')
      const response = await result.json();
      this.userList = response;
      // console.log(this.userList);
      const result1 = await fetch('http://localhost:5121/api/UserAccount/getUserAccountTempate')
      const response1 = await result1.json();
      this.genders = response1.userTemplate[0].genders
      this.departments = response1.userTemplate[0].departments
      this.rules = response1.userTemplate[0].rules
      this.branches = response1.userTemplate[0].branches
      this.statuses = response1.userTemplate[0].statuses
      // console.log('userTemplate: ',response1.userTemplate);
    },
    // async getUserAccountModel() {
    //   const result = await fetch('http://localhost:5121/api/UserAccount/getUserAccountTempate')
    //   const response = await result.json();
    //   this.userModel = response.userTemplate[0].userAccount;
    //   console.log('userTemplate: ',response.userTemplate[0].userAccount);
    // }
  }
})

