import TabDetail from './tab-detail/index.vue';
import ReloadButton from './reload-button/index.vue';
import GlobalButton from './global-button/index.vue';

export { TabDetail, ReloadButton ,GlobalButton};
