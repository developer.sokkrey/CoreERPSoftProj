// 后端接口返回的数据类型

/** 后端返回的用户权益相关类型 */
declare namespace ApiAuth {
  /** 返回的token和刷新token */
  interface Token {
    token: string;
    refreshToken: string;
  }
  /** 返回的用户信息 */
  type UserInfo = Auth.UserInfo;
}

/** 后端返回的路由相关类型 */
declare namespace ApiRoute {
  /** 后端返回的路由数据类型 */
  interface Route {
    /** 动态路由 */
    routes: AuthRoute.Route[];
    /** 路由首页对应的key */
    home: AuthRoute.AllRouteKey;
  }
}
declare namespace ApiuserAccount{
  interface userAccount {
    userAcccountID: string,
    code: string,
    name: string,
    ruleName: string,
    companyName: string,
    branchName: string,
    gender: number,
    genderValue: string,
    status: number,
    statusValue: number,
    departmentName: string,
  }
}

declare namespace ApiUserManagement {
  interface User {
    id: string;
    userName: string | null;
    age: number | null;
    gender: '0' | '1' | null;
    phone: string;
    email: string | null;
    userStatus: '1' | '2' | '3' | '4' | null;
  }
}
declare namespace ApiUserModel {
  interface UserModel {
    name: string | null;
    code: string | null;
    userName: string | null;
    password: string | null;
    confirmPassword: string | null;
    gender: '0' | '1' | '2' | '3' | null;
    ruleId: string | null ;
    branchId: string | null;
    departmentId: string | null;
    userStatus: '1' | '2' | '3' | '4' | null;
  }
}
