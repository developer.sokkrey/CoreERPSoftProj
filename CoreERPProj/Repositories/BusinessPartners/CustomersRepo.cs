﻿using CoreERPProj.DataApp;
using CoreERPProj.Models.BusinessPartners;
using CoreERPProj.Models.UserAccounts;
using CoreERPProj.Repositories.UserAccounts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;

namespace CoreERPProj.Repositories.BusinessPartners
{
    public interface ICustomers
    {
        void CreateCustomerList(List<Customer> customerList);
    }
    public class CustomersRepo : ICustomers
    {
        private readonly DataContext _dataContext;
        private readonly string? _connectionString;
        public CustomersRepo(DataContext dataContext)
        {
            _dataContext = dataContext;
            _connectionString = dataContext.Database.GetConnectionString();
        }
        public void CreateCustomerList(List<Customer> customerList)
        {
            string jsonString = JsonConvert.SerializeObject(customerList, Formatting.None);
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("sp_ParseJSON", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    // Pass the JSON string as a parameter
                    cmd.Parameters.AddWithValue("@json", jsonString);
                    // Execute the stored procedure
                    SqlDataReader reader = cmd.ExecuteReader();
                    // Do something with the result
                }
            }
        }
 
    }
}
