﻿using CoreERPProj.DataApp;
using CoreERPProj.Models.Branches;
using CoreERPProj.Models.Jwt;
using CoreERPProj.Models.UserAccounts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Reflection.PortableExecutable;

namespace CoreERPProj.Repositories.Branches
{
    public interface IBranches
    {
        IEnumerable<Branch>? GetAllBranches();
    }
    public class BranchesRepo : IBranches
    {
        private readonly ILogger<BranchesRepo> _logger;
        private readonly DataContext _dataContext;
        private readonly string? _connectionString;
        public BranchesRepo(ILogger<BranchesRepo> logger,DataContext dataContext)
        {
            _logger = logger;
            _dataContext = dataContext;
            _connectionString = dataContext.Database.GetConnectionString();
        }
        public IEnumerable<Branch>? GetAllBranches()
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                string compID = "93cfc975-517d-43c8-841b-766ec4d53f79";
                string query = "SELECT * FROM GetAll_Branches('"+ compID + "')";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }
                string json = JsonConvert.SerializeObject(ds.Tables[0], Formatting.Indented);
                List<Branch>? branch = JsonConvert.DeserializeObject<List<Branch>>(json);
                return branch;
            }
        }
    }
}
