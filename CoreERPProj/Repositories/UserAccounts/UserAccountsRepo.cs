﻿using CoreERPProj.DataApp;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;
using CoreERPProj.Models.UserAccounts;
using CoreERPProj.Models.ViewModels.UserAccounts;
using Microsoft.AspNetCore.Http.HttpResults;

namespace CoreERPProj.Repositories.UserAccounts
{
    public interface IUserAccounts
    {
        Task<DataTable> GetAllUserAccountsAsync();
        IEnumerable<object> GetAllUserAccounts();
        Task<string?> GetUserAccountTempate();
    }
    public class UserAccountsRepo : IUserAccounts
    {
        private readonly ILogger<UserAccountsRepo> _logger;
        private readonly DataContext _dataContext;
        private readonly string? _connectionString;
        public UserAccountsRepo(ILogger<UserAccountsRepo> logger, DataContext dataContext)
        {
            _logger = logger;
            _dataContext = dataContext;
            _connectionString = dataContext.Database.GetConnectionString();
        }

        public async Task<DataTable> GetAllUserAccountsAsync()
        {
            using SqlConnection cn = new SqlConnection(_connectionString);
            await cn.OpenAsync();
            using SqlCommand cmd = cn.CreateCommand();
            // cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM get_UserAccountsAll()";
            // cmd.Parameters.AddWithValue("@CompID", compID);
            using SqlDataAdapter sda = new(cmd);
            await cmd.ExecuteNonQueryAsync();
            DataTable dt = new DataTable();
            sda.Fill(dt);
            return dt;
        }
        public IEnumerable<object> GetAllUserAccounts()
        {
           using (SqlConnection con = new  SqlConnection(_connectionString))
           {
               DataTable dt = new DataTable();
               string compID = "93cfc975-517d-43c8-841b-766ec4d53f79";
               string query = "SELECT * FROM get_UserAccountsAll('" + compID + "')";
               using (SqlCommand cmd = new SqlCommand(query))
               {
                   cmd.Connection = con;    
                   using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                   {
                       sda.Fill(dt);
                   }
               }
               string userAccountsJoson = JsonConvert.SerializeObject(dt, Formatting.None);
               List<UserAccountViewModel> userAccounts = JsonConvert.DeserializeObject<List<UserAccountViewModel>>(userAccountsJoson) ?? new List<UserAccountViewModel>();
               return userAccounts;
           }
        }

        public async Task<string?> GetUserAccountTempate(){
            string? jsonData ="";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                // Specify the function call in the SQL query
                string sqlQuery = "SELECT dbo.get_UserAccountTempate() AS JsonData";
                using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                // Assuming the JSON result is in the "JsonData" column
                                 jsonData = reader["JsonData"].ToString();
                                // Process the JSON data as needed
                                // Console.WriteLine(jsonData);
                            }
                        }
                        else
                        {
                            Console.WriteLine("No data returned.");
                        }
                    }
                }
            }
            return jsonData;
        }
    }
}
